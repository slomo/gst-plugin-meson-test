# Simple GStreamer plugin in Rust example

This is a demo for Meson's Cargo support. It can be built with both Cargo and Meson:
- cargo build
- meson setup builddir && ninja -C builddir

At the time of writing it requires unmerged Meson PR:
- https://github.com/mesonbuild/meson/pull/12363
