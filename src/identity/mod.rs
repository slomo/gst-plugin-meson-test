use gst::glib;
use gst::prelude::*;

mod imp;

glib::wrapper! {
    pub struct Identity(ObjectSubclass<imp::Identity>) @extends gst::Element, gst::Object;
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(
        Some(plugin),
        "rsidentity",
        gst::Rank::NONE,
        Identity::static_type(),
    )
}
