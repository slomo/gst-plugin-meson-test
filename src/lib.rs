use gst::glib;

mod identity;

fn plugin_init(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    identity::register(plugin)?;
    Ok(())
}

gst::plugin_define!(
    mesontest,
    "GStreamer meson/Rust plugin test",
    plugin_init,
    "0.0.1",
    "MIT/X11",
    "gst-plugin-meson-test",
    "gst-plugin-meson-test",
    "https://gitlab.freedesktop.org/slomo/gst-plugin-meson-test"
);
